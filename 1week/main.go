package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	// "path/filepath"
	// "strconv"
	// "strings"
)

/*
go run main.go testdata
go run main.go testdata -f
*/

func dirTree(out io.Writer, path string, printFiles bool) error {
	// fmt.Printf("\n \n printFiles %v  %+v \n \n ", printFiles, path)
	rootDirectory := path ///home/redhat/local_project/go_project/src/tasksGo/1week/

	files, err1 := ioutil.ReadDir(rootDirectory)
	if err1 != nil {
		log.Fatal(err1)
	}

	for _, file := range files {
		fmt.Println("\n ", file.Name(), file.IsDir(), file.Size(), "\n ")
	}

	fmt.Println("\n \n ")

	// err := filepath.Walk(rootDirectory, func(path string, info os.FileInfo, err error) error {
	// 	if err != nil {
	// 		fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", rootDirectory, err)
	// 		return err
	// 	}

	// 	pathDir := strings.Replace(path, rootDirectory, "", 1)
	// 	if pathDir == "" {
	// 		return nil
	// 	}

	// 	// fmt.Printf("Name: %v \n Size: %v \n Mode: %v \n ModTime: %v \n ", info.Name(), info.Size(), info.Mode(), info.ModTime())

	// 	if !printFiles {
	// 		if info.IsDir() {
	// 			// depthNesting := strings.Count(pathDir, "/")
	// 			// strInsert := ""
	// 			// // if depthNesting == 1 {
	// 			// // 	strInsert = "├───"
	// 			// // }

	// 			// switch depthNesting {
	// 			// case 1:
	// 			// 	strInsert = "├───"
	// 			// case 2:
	// 			// 	strInsert = "│\t ├───"
	// 			// case 3:
	// 			// 	strInsert = "│\t │\t├───"
	// 			// case 4:
	// 			// 	strInsert = "│\t │\t │\t├───"
	// 			// case 5:
	// 			// 	// some work
	// 			// case 6:
	// 			// 	// some work
	// 			// default:
	// 			// 	// some work
	// 			// }

	// 			fmt.Printf("%+v \n", info.Name())
	// 		}
	// 	} else {
	// 		if info.IsDir() {
	// 			fmt.Printf("%+v  \n", info.Name())
	// 		} else {
	// 			size := ""
	// 			if info.Size() == 0 {
	// 				size = "(empty)"
	// 			} else {
	// 				size = "(" + strconv.Itoa(int(info.Size())) + "b)"
	// 			}

	// 			fmt.Printf("%+v %+v \n", info.Name(), size)
	// 		}
	// 	}

	// 	return nil
	// })

	// if err != nil {
	// 	fmt.Printf("error walking the path %q: %v\n", rootDirectory, err)
	// }
	return nil
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
